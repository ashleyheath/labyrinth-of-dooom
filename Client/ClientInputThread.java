import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Modified from Coursework 2 to call methods in the GUI.
 * Thread that handles communication from the server to the client.
 */
public class ClientInputThread extends Thread
{
  // CODES FROM SERVER
  private static final String WIN = "WIN";
  private static final String LOSE = "LOSE";
  private static final String PONG = "PONG";
  private static final String STARTTURN = "STARTTURN";
  private static final String ENDTURN = "ENDTURN";
  private static final String GOAL_ = "GOAL ";
  private static final String TREASUREMOD_ = "TREASUREMOD ";
  private static final String MESSAGE_ = "MESSAGE ";
  private static final String CHANGE = "CHANGE";
  private static final String  SUCCEED_PICKUP_ = "SUCCEED PICKUP ";

  private Socket socketToServer;
  private BufferedReader input;
  
  private static final int HISTORY_MAX_SIZE = 50;
  private ArrayList<String> history = new ArrayList<String>(HISTORY_MAX_SIZE);
  
  private LODCharacter character;
  
  private ClientOutputThread outputThread;
  private ClientGUI gui;
  
  
  /**
   * Constuctor.
   * @param socketToServer the socket to the server.
   * @param character      the character object storing data about this client's
   *                       character in game.
   * @param cot            the output thread to send messages to the server 
   *                       through.
   * @param gui            the GUI to make calls to based on the codes from the 
   *                       server.
   */
  public ClientInputThread(Socket socketToServer, LODCharacter character, 
                           ClientOutputThread cot, ClientGUI gui) 
                           throws IOException
  {
    this.socketToServer = socketToServer;
    this.input = new BufferedReader(new InputStreamReader(
    																socketToServer.getInputStream()));
    this.character = character;
    this.outputThread = cot;
    this.gui = gui;
  }
  
  /**
   * Tries to close the socket to the server, prints an error message if it
   * cannot.
   */
  public void kill()
  {
    try
    {
      this.socketToServer.close();
    }
    catch (IOException e)
    {
      System.out.println("ERROR: Could not close socket to server.");
    }
  }
  
  public LODCharacter getCharacter()
  {
    return this.character;
  }
  
  /**
   * Get the next string sent to this client by the server.
   * @return the next communication from the server or null if there is an 
   *         IOException when fetching the string.
   */
  protected String getString()
  {
    String returnStr;
    try
    {
      returnStr = this.input.readLine();
    }
    catch (IOException e)
    {
      returnStr = null;
    }
    return returnStr;
  }
  
  /**
   * Returns the input from server history.
   * @return an ArrayList of Strings containing the history of 
   *         communications from the server.
   */
  synchronized public ArrayList<String> getHistory()
  {
    return this.history;
  }
  
  /**
   * Add a new entry to the history of communcations from the server.
   * @param newEntry the string to add to the history.
   */
  synchronized protected void updateHistory(String newEntry)
  {
    if (this.history.size() >= HISTORY_MAX_SIZE)
    {
      this.history.remove(0);
    }
    this.history.add(newEntry);
  }
  
  /**
   * Constanly loop and read messages from the server, taking necessary action
   * when certain codes are recieved.
   */
  public void run()
  { 
    while (true)
    {
    	String messageFromServer = this.getString();
      if (messageFromServer != null)
      {
        this.updateHistory(messageFromServer);
        // WIN
        if (messageFromServer.equals(WIN))
        {
          this.gui.gameWin();
          this.character.setGameOver(true);
        }
        // LOSE
        else if (messageFromServer.equals(LOSE))
        {
          this.gui.gameLose();
          this.character.setGameOver(true);
        }
        // START TURN
        else if (messageFromServer.equals(STARTTURN))
        {
        	this.character.setTurn(true);
          this.gui.updateTurn(this.character);
        }
        // END TURN
        else if (messageFromServer.equals(ENDTURN))
        {
        	this.character.setTurn(false);
          this.gui.updateTurn(this.character);
        }
        // GOAL
        else if (messageFromServer.length() >= 6
                 && messageFromServer.indexOf(GOAL_) == 0)
        {
        	try
          {
          	int messageLength = messageFromServer.length();
          	String gold = messageFromServer.substring(5, messageLength);
            int goldRequired = Integer.parseInt(gold);
            this.character.setGoldRequired(goldRequired);
            this.gui.updateGoldRequired(this.character);
          }
          catch (NumberFormatException e)
          {
          }
        }
        // TREASURE MOD
        else if (messageFromServer.length() >= 13
                 && messageFromServer.indexOf(TREASUREMOD_) == 0)
        {
        	try
          {
          	int messageLength = messageFromServer.length();
          	String gold = messageFromServer.substring(12, messageLength);
            int goldChange = Integer.parseInt(gold);
            this.character.increaseGoldHeld(goldChange);
            this.gui.updateInventory(this.character);
          }
          catch (NumberFormatException e)
          {
          }
        }
        // SUCCESS
        else if (messageFromServer.indexOf(SUCCEED_PICKUP_) == 0)
        {
        	int codeLength = SUCCEED_PICKUP_.length();
        	int messageLength = messageFromServer.length();
        	String pickupCode = messageFromServer.substring(codeLength,
        																								  messageLength);
          if (pickupCode.equals("SWORD"))
          {
          	this.character.setSword(true);
          }
          else if (pickupCode.equals("ARMOUR"))
          {
          	this.character.setArmour(true);
          }
          else if (pickupCode.equals("LANTERN"))
          {
          	this.character.setLantern(true);
          }
          this.gui.updateInventory(this.character);
        }
        // PONG
        else if (messageFromServer.equals(PONG))
        {
        }
        // MESSAGE
        else if (messageFromServer.indexOf(MESSAGE_) == 0)
        {
          String message = messageFromServer.substring(7) + "\n";
          this.gui.updateChat(message);
        }
        // CHANGE
        else if (messageFromServer.equals(CHANGE))
        {
          new MapBuilder(this.outputThread, this.gui, this.history,
                         this.character).start();
        }
      }
    }
  }
}
