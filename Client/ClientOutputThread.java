import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Modified from Coursework 2.
 * Thread that handles communication from the client to the server.
 */
public class ClientOutputThread extends Thread
{
  protected Socket socketToServer;
  protected PrintWriter output;
  
  /**
   * Constructor.
   * @param socketToServer the socket to the server.
   */
  public ClientOutputThread(Socket socketToServer) throws IOException
  {
    this.socketToServer = socketToServer;
    this.output = new PrintWriter(this.socketToServer.getOutputStream(), true);
  }
  
  /**
   * Sends a string to the server.
   * @param message the string to send.
   */
  public void sendString(String message)
  {
    this.output.println(message);
    this.output.flush();
  }
  
  /**
   * In this version, does nothing as all output is done on prompt from the 
   * GUI.
   */
  public void run()
  {
  }
}
