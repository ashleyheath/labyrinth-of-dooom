import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;

/**
 * The class that handles the client's connections to the server.
 */
public class Client
{
  private Socket socketToServer;
  private SocketAliveCheckerThread liveCheckThread;
  private ClientOutputThread output;
  private ClientInputThread input;
  private MapBuilder mapBuilder;
  private ClientGUI gui;
  
  /**
   * Handles setting up a new client object.
   * @param serverAddress         the address of the server to connect to.
   * @param port                  the port to try and connect on.
   * @param gui                   the GUI object to communicate with.
   * @return                      a new client object.
   * @throws IOEXception          if an IO error occurs when creating the 
   *                              socket.
   * @throws UnknownHostException if there is no server at the given address and
   *                              port to recieve the connection.
   */
  public static Client newClient(String serverAddress, int port, ClientGUI gui) 
                                throws IOException, UnknownHostException
  {
    Socket newSocket = new Socket(serverAddress, port);
    LODCharacter playerCharacter = new LODCharacter();
    ClientOutputThread out = new ClientOutputThread(newSocket);
    ClientInputThread in = new ClientInputThread(newSocket, playerCharacter,
                                                 out, gui);    
    return new Client(newSocket, in, out, gui, playerCharacter);
  }
  
  /**
   * Constructor.
   * @param socketToServer the socket to the server.
   * @param cit            the client input thread.
   * @param cot            the client output thread.
   */
  private Client(Socket socketToServer, ClientInputThread cit,
                 ClientOutputThread cot, ClientGUI gui, LODCharacter character)
                 throws IOException
  {
    this.socketToServer = socketToServer;
    this.output = cot;
    this.output.start();
    this.input = cit;
    this.input.start();
    this.liveCheckThread = new SocketAliveCheckerThread(cit, cot, gui, 
                                                        character);
    this.liveCheckThread.start();
    this.gui = gui;
  }
  
  /**
   * Shutdown the client, attempting to safely close the socket connection to
   * the server.
   */
  public void shutDown()
  {
    try
    {
      this.socketToServer.close();
    }
    catch (IOException e)
    {
      System.out.println("ERROR: Failed to close socket to server.");
    }
  }
  
  // METHODS CALLED BY INTERFACE METHODS
  
  /**
   * Calls the fetchAndCreateMap() method in the input thread to update the 
   * locally stored map.
   */
  public void fetchAndCreateMap()
  {
    new MapBuilder(this.output, this.gui, this.input.getHistory(),
                         this.input.getCharacter()).start();
  }
  
  /**
   * Returns the copy of the map stored localy in the LODCharacter in the input
   * thread.
   * @return a String array that is the local map.
   */
  public String [][] getMapArray()
  {
    return this.input.getCharacter().getLocalMap();
  }
  
  /**
   * Returns the LODCharacter stored by the input thread.
   * @return the local LODCharacter.
   */
  public LODCharacter getCharacter()
  {
    return this.input.getCharacter();
  }
  
  /**
   * Sends a string to the server.
   * @param message the message to send.
   */
  public void sendMessageToServer(String message)
  {
    this.output.sendString(message);
  }
}
