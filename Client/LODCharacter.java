/**
 * Modified from Coursework 2.
 * This is where the client stores anything it knows about the game world.
 */
public class LODCharacter
{
  protected int goldRequiredToWin = -1;
  
  protected int goldHeld = 0;
  protected String [][] localMap = null;
  protected int [][] otherPlayersPositions = null;
  
  protected boolean hasArmour = false;
  protected boolean hasSword = false;
  protected boolean hasLantern = false;
  
  protected boolean turn = false;  
  
  protected boolean gameOver = false;
  
  public LODCharacter()
  {
  }
  
  public void setGameOver(boolean gameOver)
  {
    this.gameOver = gameOver;
  }
  
  public boolean getGameOver()
  {
    return this.gameOver;
  }
  
  public void setGoldRequired(int gold)
  {
    this.goldRequiredToWin = gold;
  }
  
  public int getGoldRequired()
  {
    return this.goldRequiredToWin;
  }
  
  public void setLocalMap(String [][] localMap)
  {
    this.localMap = localMap;
  }
  
  public String [][] getLocalMap()
  {
    return this.localMap;
  }
  
  /**
   * Take the relative positions of other players and convert them to work
   * with this implementation of the local coordinate system.
   * @param positions an array with the pairs of relative coordinates.
   */
  public void setPlayerRelativePositions(int [][] positions)
  {
    if (positions != null)
    {
      this.otherPlayersPositions = new int [positions.length][];
      for (int i = 0; i < positions.length; i++)
      {
        int actualX = this.getLocalXPos() - positions[i][0];
        int actualY = this.getLocalYPos() - positions[i][1];
        this.otherPlayersPositions[i] = new int [] {actualX, actualY};
      }
    }
  }
  
  public boolean otherPlayerAtPosition(int x, int y)
  {
    for (int i = 0; i < this.otherPlayersPositions.length; i++)
    {
      if (x == this.otherPlayersPositions[i][0] &&
          y == this.otherPlayersPositions[i][1])
      {
        return true;
      }
    }
    return false;
  }
  
  public boolean hasArmour()
  {
    return this.hasArmour;
  }
  
  public boolean hasSword()
  {
    return this.hasSword;
  }
  
  public boolean hasLantern()
  {
    return this.hasLantern;
  }
  
  public void setArmour(boolean b)
  {
    this.hasArmour = b;
  }
  
  public void setSword(boolean b)
  {
    this.hasSword = b;
  }
  
  public void setLantern(boolean b)
  {
    this.hasLantern = b;
  }
  
  public void setTurn(boolean b)
  {
    this.turn = b;
  }
  
  public boolean getTurn()
  {
    return this.turn;
  }
  
  /**
   * Get the character's y coordinate in relation to the current local map. The
   * character is always at the centre of the local map.
   * @return the local y coordinate.
   */
  public int getLocalYPos()
  {
    return this.localMap.length / 2;
  }
  
  /**
   * Get the character's x coordinate in relation to the current local map. The
   * character is always at the centre of the local map.
   * @return the local x coordinate.
   */
  public int getLocalXPos()
  {
    return this.localMap[this.getLocalYPos()].length / 2;
  }
  
  public void increaseGoldHeld(int amountToAdd)
  {
    this.goldHeld += amountToAdd;
  }
  
  public int getGoldHeld()
  {
    return this.goldHeld;
  }
  
  /**
   * Returns the DONE instruction code. Intended to be overridden by classes
   * that extend the Character class so that when the GameLogic class calls them
   * the characters perform their necessary actions. Intended to return the
   * instruction code that a character wants the GameLogic class to act upon.
   */
  public String act()
  {
    return "DONE";
  }
  
  /**
   * Takes a pair of changes (increment/decrement/no change) to be made to the
   * character's current coordinates. Returns an array of strings stating
   * whether the move is valid and any error messages provided.
   * @param directionChanges the adjustments to be made to the character's
   *                         current coordinates.
   * @return                 an array of strings of length 2. 0th index contains
   *                         either VALID or INVALID depending on whether the
   *                         provided changes are legal. The 1st index contains
   *                         any error messages or additional information to
   *                         help explain the reason for INVALID being returned.
   */
  protected String [] validateMove(int [] directionChanges)
  {
    String [] returnArray = new String [2];
    
    int newX = this.getLocalXPos() + directionChanges[0];
    int newY = this.getLocalYPos() + directionChanges[1];
    
    //If new position is still on the map
    if (newY >= 0 && newY < this.localMap.length
        && newX >= 0 && newX < this.localMap[newY].length)
    {
      String newTile = this.localMap[newY][newX];
      //If new position is not a wall tile
      if (newTile.equals("#"))
      {
        returnArray[0] = "INVALID";
        returnArray[1] = "Can't walk through walls.";
      }
      /*If new position is not a null tile (i.e. not part of the map just part
        of the local map array).*/
      else if (newTile.equals(" "))
      {
        returnArray[0] = "INVALID";
        returnArray[1] = "That would take you off the map.";
      }
      // Don't walk into other players
      else
      {
        boolean matchFound = false;
        for (int i = 0; i < this.otherPlayersPositions.length; i++)
        {
          if (this.otherPlayersPositions[i][0] == newX
              && this.otherPlayersPositions[i][1] == newY)
          {
            matchFound = true;
            break;
          }
        }
        if (matchFound)
        {
          returnArray[0] = "INVALID";
          returnArray[1] = "That would take you into another player.";
        }
        else
        {
          returnArray[0] = "VALID";
          returnArray[1] = "The move is legal.";
        }
      }
    }
    else
    {
      returnArray[0] = "INVALID";
      returnArray[1] = "That would take you off the map.";
    }
    return returnArray;
  }
}
