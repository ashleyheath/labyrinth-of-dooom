import java.net.Socket;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Modified from Courswork 2 to notify GUI of loss of service.
 * Sends a PING message to the server at intervals of timeBetweenPings
 * miliseconds and checks to see if it recieves a PONG message in reply in
 * order to determine if the server is still up.
 */
public class SocketAliveCheckerThread extends Thread
{
  private static final int TIME_BETWEEN_PINGS = 100;
  private static final int PINGS_TO_MISS = 20;
  private ClientOutputThread output;
  private ClientInputThread  input;
  private ClientGUI          gui;
  private LODCharacter       character;
  /**
   * Constructor.
   * @param input  the client's input thread.
   * @param output the client's output thread.
   */
  public SocketAliveCheckerThread(ClientInputThread input,
                                  ClientOutputThread output, ClientGUI gui,
                                  LODCharacter character) throws IOException
  {
    this.output = output;
    this.input = input;
    this.gui = gui;
    this.character = character;
  }
  
  /**
   * Perform the check to see if the server is still there.
   */
  public void run()
  {
    int pingsMissed = 0;
    while (true)
    {
      this.output.sendString("PING");
      try
      {
        Thread.sleep(TIME_BETWEEN_PINGS);
        boolean pongFound = false;
        ArrayList<String> history = this.input.getHistory();
        for (int i = history.size() - 1; i >= 0; i--)
        {
          String line = history.get(i);
          if (line.equals("PONG"))
          {
            pongFound = true;
            history.remove(i);
            break;
          }
        }
        if (!pongFound)
        {
          pingsMissed++;
        }
        if (pingsMissed >= PINGS_TO_MISS)
        {
          /* If the game has not finished but the connection is lost, display
             this in the GUI. */
          if (!this.character.getGameOver())
          {
            this.gui.connectionLost();
          }
          break;
        }
      }
      catch (InterruptedException e)
      {
      }
    } 
  }
}
