import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.io.IOException;
import java.io.File;

/**
 * Class to create the label depicting the local player, to be displayed at the
 * center of the map.
 */
public class ClientPlayerPainter
{
  private static final int IMAGE_WIDTH = 50;
  private static final int IMAGE_HEIGHT = 50;
  
  private static BufferedImage PLAYER_IMAGE = null;
  private static BufferedImage SWORD_IMAGE = null;
  private static BufferedImage ARMOUR_IMAGE = null;
  private static BufferedImage LANTERN_IMAGE = null;
  
  /**
   * Loads the images stored in ~/Tiles/Player/. 
   * @throws IOException if one of the images cannot be found.
   */
  public static void loadPlayerImages() throws IOException
  {
    PLAYER_IMAGE = ImageIO.read(new File("Tiles/Player/player.png"));
    SWORD_IMAGE = ImageIO.read(new File("Tiles/Player/sword.png"));
    ARMOUR_IMAGE = ImageIO.read(new File("Tiles/Player/armour.png"));
    LANTERN_IMAGE = ImageIO.read(new File("Tiles/Player/lantern.png"));
  }
  
  /**
   * Creates a label showing the player based on the provided LODCharacter.
   * @param character the LODCharacter object.
   * @return          a JLabel with the composited image of the local player.
   */
  public static JLabel createPlayerLabel(LODCharacter character)
  {
    BufferedImage layeredImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT,
    																							BufferedImage.TYPE_INT_ARGB);
    
    Graphics layeredGraphics = layeredImage.getGraphics();
    layeredGraphics.drawImage(PLAYER_IMAGE, 0, 0, null);
    if (character.hasSword())
    {
      layeredGraphics.drawImage(SWORD_IMAGE, 0, 0, null);
    }
    if (character.hasArmour())
    {
      layeredGraphics.drawImage(ARMOUR_IMAGE, 0, 0, null);
    }
    if (character.hasLantern())
    {
      layeredGraphics.drawImage(LANTERN_IMAGE, 0, 0, null);
    }
    return new JLabel(new ImageIcon(layeredImage));
  }
}
