import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.text.DefaultStyledDocument;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;

public class ClientGUI extends JFrame
{
  private static final String GOLD_TILE          = "G";
  private static final String SWORD_TILE         = "S";
  private static final String ARMOUR_TILE        = "A";
  private static final String HEALTH_POTION_TILE = "H";
  private static final String LANTERN_TILE       = "L";
  private static final String FLOOR_TILE         = ".";
  private static final String WALL_TILE          = "#";
  private static final String FOG_TILE           = "X";
  private static final String EXIT_TILE          = "E";

  private int BUFFER = 5;
  private String blankSpace = "                                         ";

  private Client client;

  private Container     container;
  private GridBagLayout layout;
  private GUIListener   listener;
  
  // MENU COMPONENTS
  
  private int chosenPort       = -1;
  private String chosenAddress = "";
    
  private JTextField addressField = new JTextField(20);
  private JTextField portField    = new JTextField(20);
  private JTextField nameField    = new JTextField(20);
  private JButton connectButton   = new JButton("Connect");
  
  private JLabel addressLabel = new JLabel("Server Address:", JLabel.RIGHT);
  private JLabel portLabel    = new JLabel("Server Port:", JLabel.RIGHT);
  private JLabel nameLabel    = new JLabel("Player Name:", JLabel.RIGHT);
  
  private JLabel addressErrorLabel = new JLabel(blankSpace, JLabel.LEFT);
  private JLabel portErrorLabel    = new JLabel(blankSpace, JLabel.LEFT);
  private JLabel nameErrorLabel    = new JLabel(blankSpace, JLabel.LEFT);
  private JLabel generalErrorLabel = new JLabel(blankSpace, JLabel.LEFT);
  
  // GAME COMPONENTS
  
  private JPanel           mapPanel = new JPanel(new GridBagLayout());
  private JLabel           goldTile;
  private JLabel           swordTile;
  private JLabel           armourTile;
  private JLabel           healthPotionTile;
  private JLabel           lanternTile;
  private JLabel           floorTile;
  private JLabel           wallTile;
  private JLabel           exitTile;
  private JLabel           unknownTile;
  private JLabel           fogTile;
  private JLabel           playerTile;
  
  private JPanel           gameplayButtonPanel = new JPanel(new GridBagLayout());
  private JButton          pickupButton = new JButton("Pickup");
  private BasicArrowButton northButton = new BasicArrowButton(SwingConstants.NORTH);
  private BasicArrowButton eastButton = new BasicArrowButton(SwingConstants.EAST);
  private BasicArrowButton southButton = new BasicArrowButton(SwingConstants.SOUTH);
  private BasicArrowButton westButton = new BasicArrowButton(SwingConstants.WEST);
  
  private JPanel           miscButtonPanel = new JPanel(new GridBagLayout());
  private JButton          quitButton      = new JButton("Quit Game");
  private JButton          endturnButton   = new JButton("End Turn");
  
  private JPanel           infoPanel = new JPanel(new GridBagLayout());
  private JLabel           goldInventory = new JLabel(blankSpace, JLabel.CENTER);
  private JLabel           swordInventory = new JLabel(blankSpace, JLabel.CENTER);
  private JLabel           armourInventory = new JLabel(blankSpace, JLabel.CENTER);
  private JLabel           lanternInventory = new JLabel(blankSpace, JLabel.CENTER);
  private JLabel           goldDisplay = new JLabel(blankSpace, JLabel.CENTER);
  private JLabel           turnDisplay = new JLabel(blankSpace, JLabel.CENTER);
  
  private JPanel           chatPanel = new JPanel(new GridBagLayout());
  private JTextPane        chatOutput = new JTextPane();
  private JScrollPane      chatOutputScrollPane = new JScrollPane(this.chatOutput);
  private JTextField       chatInput = new JTextField(20);
  private DefaultStyledDocument conversation = new DefaultStyledDocument();
   
  /**
   * Constructor. Sets up the graphical user interface.
   */
  public ClientGUI()
  {
    this.setTitle("The Labyrinth of Dooom");
    this.setDefaultCloseOperation(EXIT_ON_CLOSE);		
		
    this.container = this.getContentPane();
    this.layout = new GridBagLayout();
    this.container.setLayout(this.layout);
    
    this.setUpMenu();

		this.pack();
		this.setVisible(true);
		this.setResizable(false);
  }
  
  /**
   * Update the main ClientGUI frame.
   */
  private void updateFrame()
  {
    this.pack();
    this.validate();
    this.repaint();
  }
  
  /**
   * Listens for events from the setup menu components.
   */
  private class MenuListener implements ActionListener
  {
    private ClientGUI guiReference;
    
    /**
     * Constructor.
     * @param guiReference the ClientGUI object to be passed to the new Client.
     */
    public MenuListener(ClientGUI guiReference)
    {
      this.guiReference = guiReference;
    }
    
    /**
     * Takes action based on the events recieved from the menu components.
     * @param e the event that has happened.
     */
    public void actionPerformed(ActionEvent e)
    {
      // Reset the error labels
      addressErrorLabel.setText(blankSpace);
      portErrorLabel.setText(blankSpace);
      nameErrorLabel.setText(blankSpace);
      generalErrorLabel.setText(blankSpace);
      
      Object source = e.getSource();
      if (source == addressField || source == portField ||
          source == nameField || source == connectButton)
      {
        String address = addressField.getText();
        String port = portField.getText();
        String name = nameField.getText();
        
        // Check that all the fields have something in them
        boolean contentInAll = true;
        if (address.equals("") || address == null)
        {
          addressErrorLabel.setText("You need to enter an address.");
          contentInAll = false;
        }
        if (port.equals("") || port == null)
        {
          portErrorLabel.setText("You need to enter a port number.");
          contentInAll = false;
        }
        if (name.equals("") || name == null)
        {
          nameErrorLabel.setText("You need to enter a name for your player.");
          contentInAll = false;
        }
        // Validate the contents off the fields
        if (contentInAll)
        {
          try
          {
            chosenPort = Integer.parseInt(port);
            chosenAddress = address;
            /* Try and create a Client that connects to the server and if 
               successful remove the menu components and set out the game's
               graphical components. */
            try
            {
              client = Client.newClient(chosenAddress, chosenPort, 
                                        this.guiReference);
              // Send the player's name to the server
              client.sendMessageToServer("HELLO " + name);
              
              // Delete the menu's components
              container.remove(addressField);
              container.remove(portField);
              container.remove(nameField);
              container.remove(connectButton);
              container.remove(addressLabel);
              container.remove(portLabel);
              container.remove(nameLabel);
              container.remove(addressErrorLabel);
              container.remove(portErrorLabel);
              container.remove(nameErrorLabel);
              container.remove(generalErrorLabel);
                
              // Set up the game screen
              listener = new GUIListener();
              setUpMap();
              setUpGameplayButtonPanel();
              setUpMiscButtonPanel();
              setUpInfoPanel();
              setUpChatPanel();
              
              updateFrame();
              
              // Fix the gameplay display components to their intial size
              gameplayButtonPanel.setPreferredSize(gameplayButtonPanel.getSize());
              miscButtonPanel.setPreferredSize(miscButtonPanel.getSize());
              infoPanel.setPreferredSize(infoPanel.getSize());
              chatPanel.setPreferredSize(chatPanel.getSize());
            
            }
            catch (IOException ioe)
            {
              generalErrorLabel.setText("Could not connect to server.");
            }
          }
          catch (NumberFormatException nfe)
          {
            portErrorLabel.setText("The port must be a number.");
          }
        }
      }
      updateFrame();
    }
  };
  
  /**
   * Set up the components for the menu that allows you to enter server details
   * and pick a name.
   */
  private void setUpMenu()
  {
    MenuListener menuListener = new MenuListener(this);  
    
    GridBagConstraints constraints = new GridBagConstraints();
    
    constraints.gridx = 1;
    
    this.addressField.addActionListener(menuListener);
    constraints.gridy = 0;
    this.container.add(this.addressField, constraints);
    this.portField.addActionListener(menuListener);
    constraints.gridy = 1;
    this.container.add(this.portField, constraints);
    this.nameField.addActionListener(menuListener);
    constraints.gridy = 2;
    this.container.add(this.nameField, constraints);
    this.connectButton.addActionListener(menuListener);
    constraints.gridy = 3;
    this.container.add(this.connectButton, constraints);
    
    constraints.gridx = 0;
    
    constraints.gridy = 0;
    this.container.add(this.addressLabel, constraints);
    constraints.gridy = 1;
    this.container.add(this.portLabel, constraints);
    constraints.gridy = 2;
    this.container.add(this.nameLabel, constraints);
    
    constraints.gridx = 2;
    
    constraints.gridy = 0;
    this.container.add(this.addressErrorLabel, constraints);
    constraints.gridy = 1;
    this.container.add(this.portErrorLabel, constraints);
    constraints.gridy = 2;
    this.container.add(this.nameErrorLabel, constraints);
    constraints.gridy = 3;
    this.container.add(this.generalErrorLabel, constraints);
  }
  
  /**
   * Listener for the components of the gameplay screen.
   */
  private class GUIListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      Object source = e.getSource();
      if (source == northButton)
      {
        client.sendMessageToServer("MOVE N");
        client.fetchAndCreateMap();
      }
      else if (source == eastButton)
      {
        client.sendMessageToServer("MOVE E");
        client.fetchAndCreateMap();
      }
      else if (source == southButton)
      {
        client.sendMessageToServer("MOVE S");
        client.fetchAndCreateMap();
      }
      else if (source == westButton)
      {
        client.sendMessageToServer("MOVE W");
        client.fetchAndCreateMap();
      }
      else if (source == pickupButton)
      {
        client.sendMessageToServer("PICKUP");
        client.fetchAndCreateMap();
      }
      else if (source == endturnButton)
      {
        client.sendMessageToServer("ENDTURN");
      }
      else if (source == quitButton)
      {
        System.exit(0);
      }
      else if (source == chatInput)
      {
        String message = chatInput.getText();
        client.sendMessageToServer("SHOUT " + message);
        updateChat("You said: " + message + "\n");
        chatInput.setText("");
      }
      updateFrame();
    }
  };
  
  /**
   * Initialise and set up the options for map panel.
   */
  private void setUpMap()
  {
    BufferedImage goldImage = null;
    BufferedImage swordImage = null;
    BufferedImage armourImage = null;
    BufferedImage healthPotionImage = null;
    BufferedImage lanternImage = null;
    BufferedImage floorImage = null;
    BufferedImage wallImage = null;
    BufferedImage exitImage = null;
    BufferedImage unknownImage = null;
    BufferedImage fogImage = null;
    BufferedImage playerImage = null;
    // Try to load the image files
    try
    {
      ClientPlayerPainter.loadPlayerImages();
    
      goldImage = ImageIO.read(new File("Tiles/gold_tile.jpg"));
      swordImage = ImageIO.read(new File("Tiles/sword_tile.jpg"));
      armourImage = ImageIO.read(new File("Tiles/armour_tile.jpg"));
      healthPotionImage = ImageIO.read(new File("Tiles/health_potion_tile.jpg"));
      lanternImage = ImageIO.read(new File("Tiles/lantern_tile.jpg"));
      floorImage = ImageIO.read(new File("Tiles/floor_tile.jpg"));
      wallImage = ImageIO.read(new File("Tiles/wall_tile.jpg"));
      exitImage = ImageIO.read(new File("Tiles/exit_tile.jpg"));
      unknownImage = ImageIO.read(new File("Tiles/unknown_tile.jpg"));
      fogImage = ImageIO.read(new File("Tiles/fog_tile.jpg"));
      playerImage = ImageIO.read(new File("Tiles/player_tile.jpg"));
    }
    catch (IOException ioe)
    {
      System.out.println("ERROR: A tile icon could not be opened, " +
                         "the icon may be missing.");
      System.exit(1);
    }
    this.goldTile = new JLabel(new ImageIcon(goldImage));
    this.swordTile = new JLabel(new ImageIcon(swordImage));
    this.armourTile = new JLabel(new ImageIcon(armourImage));
    this.healthPotionTile = new JLabel(new ImageIcon(healthPotionImage));
    this.lanternTile = new JLabel(new ImageIcon(lanternImage));
    this.floorTile = new JLabel(new ImageIcon(floorImage));
    this.wallTile = new JLabel(new ImageIcon(wallImage));
    this.exitTile = new JLabel(new ImageIcon(exitImage));
    this.unknownTile = new JLabel(new ImageIcon(unknownImage));
    this.fogTile = new JLabel(new ImageIcon(fogImage));
    this.playerTile = new JLabel(new ImageIcon(playerImage));
    
    GridBagConstraints mapConstraints = new GridBagConstraints();
    mapConstraints.gridx = 1;
    mapConstraints.gridy = 0;
    this.container.add(this.mapPanel, mapConstraints);
    
    this.client.fetchAndCreateMap();
  }
  
  /**
   * Initialise and set the options for the gameplay button panel.
   */
  private void setUpGameplayButtonPanel()
  {
    GridBagConstraints constraints = new GridBagConstraints();
    
    this.pickupButton.addActionListener(this.listener);
    constraints.gridx = 1;
    constraints.gridy = 1;
    constraints.anchor = GridBagConstraints.CENTER;
    
    this.gameplayButtonPanel.add(this.pickupButton, constraints);
    this.northButton.addActionListener(this.listener);
    constraints.gridx = 1;
    constraints.gridy = 0;
    constraints.anchor = GridBagConstraints.PAGE_START;
    this.gameplayButtonPanel.add(this.northButton, constraints);
    this.eastButton.addActionListener(this.listener);
    constraints.gridx = 2;
    constraints.gridy = 1;
    constraints.anchor = GridBagConstraints.LINE_END;
    this.gameplayButtonPanel.add(this.eastButton, constraints);
    this.southButton.addActionListener(this.listener);
    constraints.gridx = 1;
    constraints.gridy = 2;
    constraints.anchor = GridBagConstraints.PAGE_END;
    this.gameplayButtonPanel.add(this.southButton, constraints);
    this.westButton.addActionListener(this.listener);
    constraints.gridx = 0;
    constraints.gridy = 1;
    constraints.anchor = GridBagConstraints.LINE_START;
    this.gameplayButtonPanel.add(this.westButton, constraints);
    
    GridBagConstraints gameplayButtonConstraints = new GridBagConstraints();
    gameplayButtonConstraints.gridx = 1;
    gameplayButtonConstraints.gridy = 1;
    this.container.add(this.gameplayButtonPanel, gameplayButtonConstraints);
  }
  
  /**
   * Initialise and set up the options for the misc button panel.
   */
  private void setUpMiscButtonPanel()
  {
    GridBagConstraints constraints = new GridBagConstraints();
    
    constraints.gridy = 0;

    this.endturnButton.addActionListener(this.listener);
    constraints.gridx = 0;
    constraints.anchor = GridBagConstraints.LINE_START;
    this.miscButtonPanel.add(this.endturnButton);
    
    this.quitButton.addActionListener(this.listener);
    constraints.gridx = 1;
    constraints.anchor = GridBagConstraints.LINE_END;
    this.miscButtonPanel.add(this.quitButton, constraints);
    
    GridBagConstraints miscButtonConstraints = new GridBagConstraints();
    miscButtonConstraints.gridx = 1;
    miscButtonConstraints.gridy = 2;
    this.container.add(this.miscButtonPanel, miscButtonConstraints);
  }
  
  /**
   * Initialise and set up the options for the info panel.
   */
  private void setUpInfoPanel()
  {
    GridBagConstraints panelConstraints = new GridBagConstraints();
    panelConstraints.gridx = 0;
    panelConstraints.fill = GridBagConstraints.BOTH;
    
    // Set up the inventory label
    JLabel inventoryLabel = new JLabel("Inventory", JLabel.CENTER);
    
    panelConstraints.gridy = 0;
    panelConstraints.weighty = 1;
    this.infoPanel.add(inventoryLabel, panelConstraints);
    
    // Set up the inventory panel
    JPanel inventoryPanel = new JPanel(new GridBagLayout());
    
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridy = 0;
    constraints.fill = GridBagConstraints.BOTH;
    
    constraints.gridy = 0;
    inventoryPanel.add(this.goldInventory, constraints);
    
    constraints.gridy = 1;
    inventoryPanel.add(this.swordInventory, constraints);
    
    constraints.gridy = 2;
    inventoryPanel.add(this.armourInventory, constraints);
    
    constraints.gridy = 3;
    inventoryPanel.add(this.lanternInventory, constraints);
  
    panelConstraints.gridy = 1;
    panelConstraints.weighty = 5;
    this.infoPanel.add(inventoryPanel, panelConstraints);
    
    // Set up the info panel
    JPanel infoPanel = new JPanel(new GridBagLayout());
    
    constraints = new GridBagConstraints();
    constraints.gridx = 0;
    constraints.fill = GridBagConstraints.BOTH;
    
    constraints.gridy = 0;
    infoPanel.add(this.goldDisplay, constraints);
    
    constraints.gridy = 1;
    infoPanel.add(this.turnDisplay, constraints);
    
    panelConstraints.gridy = 2;
    panelConstraints.weighty = 4;
    this.infoPanel.add(infoPanel, panelConstraints);

    // Update the inventory and info data
    LODCharacter character = this.client.getCharacter();
    this.updateInventory(character);
    this.updateTurn(character);
    this.updateGoldRequired(character);
    
    GridBagConstraints infoConstraints = new GridBagConstraints();
    infoConstraints.gridx = 0;
    infoConstraints.gridy = 0;
    infoConstraints.gridheight = 3;
    infoConstraints.fill = GridBagConstraints.VERTICAL;
    this.container.add(this.infoPanel, infoConstraints);
  }
  
  /**
   * Initialise and set up the options for the chat panel.
   */
  private void setUpChatPanel()
  {
    this.chatOutput.setEditable(false);
    
    GridBagConstraints constraints = new GridBagConstraints();
    
    constraints.weightx = 1;
    
    JLabel chatLabel = new JLabel("Chat", JLabel.CENTER);
    constraints.gridy = 0;
    constraints.weighty = 1;
    constraints.fill = GridBagConstraints.BOTH;
    this.chatPanel.add(chatLabel, constraints);
    
    this.chatOutput.setDocument(this.conversation);
    int policy = JScrollPane.VERTICAL_SCROLLBAR_ALWAYS;
    this.chatOutputScrollPane.setVerticalScrollBarPolicy(policy);
    constraints.gridy = 1;
    constraints.weighty = 9;
    this.chatPanel.add(this.chatOutputScrollPane, constraints);
    
    this.chatInput.setEditable(true);
    this.chatInput.addActionListener(this.listener);
    constraints.gridy = 2;
    constraints.weighty = 1;
    this.chatPanel.add(this.chatInput, constraints);
    
    GridBagConstraints chatConstraints = new GridBagConstraints();
    chatConstraints.gridx = 2;
    chatConstraints.gridy = 0;
    chatConstraints.gridheight = 3;
    chatConstraints.fill = GridBagConstraints.VERTICAL;
    this.container.add(this.chatPanel, chatConstraints);
  }
  
  /*
   * Return a copy of a JLabel with an icon on it.
   * @param label the label to copy.
   * @return      a copy of the label.
   */
  private JLabel cloneJLabel(JLabel label)
  {
    return new JLabel(label.getIcon());
  }
  
  /**
   * Update the map graphics based on the local map of the Client's character.
   */
  public void updateMap()
  {
    this.mapPanel.removeAll();
    
    String [][] mapArray = this.client.getMapArray();
    LODCharacter character = this.client.getCharacter();
    int localX = character.getLocalXPos();
    int localY = character.getLocalYPos();
    
    for (int y = 0; y < mapArray.length; y++)
    {
      for (int x = 0; x < mapArray[y].length; x++)
      {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.fill = GridBagConstraints.BOTH;
        String tile = mapArray[y][x];
        JLabel label;
        if (character.otherPlayerAtPosition(x, y))
        {
          label = this.cloneJLabel(this.playerTile);
        }
        else if (localX == x && localY == y)
        {
          label = ClientPlayerPainter.createPlayerLabel(character);
        }
        else if (tile.equals(GOLD_TILE))
        {
          label = this.cloneJLabel(this.goldTile);
        }
        else if (tile.equals(SWORD_TILE))
        {
          label = this.cloneJLabel(this.swordTile);
        }
        else if (tile.equals(ARMOUR_TILE))
        {
          label = this.cloneJLabel(this.armourTile);
        }
        else if (tile.equals(HEALTH_POTION_TILE))
        {
          label = this.cloneJLabel(this.healthPotionTile);
        }
        else if (tile.equals(LANTERN_TILE))
        {
          label = this.cloneJLabel(this.lanternTile);
        }
        else if (tile.equals(FLOOR_TILE))
        {      
          label = this.cloneJLabel(this.floorTile); 
        }
        else if (tile.equals(WALL_TILE))
        {   
          label = this.cloneJLabel(this.wallTile);   
        }
        else if (tile.equals(FOG_TILE))
        {
          label = this.cloneJLabel(this.fogTile);
        }
        else if (tile.equals(EXIT_TILE))
        {
          label = this.cloneJLabel(this.exitTile);
        }
        else
        {  
          label = this.cloneJLabel(this.unknownTile);    
        }
        this.mapPanel.add(label, constraints);
      }
    }
    this.mapPanel.revalidate();
    this.mapPanel.repaint();
    this.updateFrame();
  }
  
  /**
   * Update the inventory display.
   * @param charcter the character containing the inventory data.
   */
  public void updateInventory(LODCharacter character)
  {
    // Update gold held
    this.goldInventory.setText("Gold held: " + character.getGoldHeld());
    // Has gold?
    if (character.hasSword())
    {
      this.swordInventory.setText("You have a sword.");
    }
    else
    {
      this.swordInventory.setText("You have no sword.");
    }
    // Has armour?
    if (character.hasArmour())
    {
      this.armourInventory.setText("You are wearing armour.");
    }
    else
    {
      this.armourInventory.setText("You have no armour.");
    }
    // Has lantern?
    if (character.hasLantern())
    {
      this.lanternInventory.setText("You have a lantern.");
    }
    else
    {
      this.lanternInventory.setText("You have no lantern.");
    }
    this.infoPanel.revalidate();
    this.infoPanel.repaint();
  }
  
  /**
   * Update the turn display.
   * @param character the character storing to turn information.
   */
  public void updateTurn(LODCharacter character)
  {
    if (character.getTurn())
    {
      this.turnDisplay.setText("It is your turn");
    }
    else
    {
      this.turnDisplay.setText("It is not your turn");
    }
    this.infoPanel.revalidate();
    this.infoPanel.repaint();
  }
  
  /**
   * Update the amount of gold required.
   * @param charcter the character storing the amount of gold required to win.
   */
  public void updateGoldRequired(LODCharacter character)
  {
    this.goldDisplay.setText(character.getGoldRequired() +
                             " gold required to win");
    this.infoPanel.revalidate();
    this.infoPanel.repaint();
  }
  
  /**
   * Add a new message to the chat pane.
   * @param newContent the string to add to the chat.
   */
  public void updateChat(String newContent)
  {
    try
    {
      int conversationLen = this.conversation.getLength();
      this.conversation.insertString(conversationLen, newContent, null);
    }
    catch (javax.swing.text.BadLocationException ble)
    {
      System.out.println("Error: Could not write message to local chat.");
    }
    // Move the scroll pane to the bottom
    this.chatOutput.setCaretPosition(this.chatOutput.getDocument().getLength());
    this.chatPanel.revalidate();
    this.chatPanel.repaint();
  }
  
  /**
   * Removes the 5 main gameplay panels from the frame.
   */
  private void removeGameComponents()
  {
    this.container.remove(this.mapPanel);
    this.container.remove(this.infoPanel);
    this.container.remove(this.chatPanel);
    this.container.remove(this.gameplayButtonPanel);
    this.container.remove(this.miscButtonPanel);
  }
  
  /**
   * Reconfigure the frame to show a single label with either an image or a
   * text message and below that a button to quit the program.
   * @param imagePath the path of the image to try and place on the label.
   * @param message   the message to display on the label if the image cannot be
   *                  loaded.
   */
  private void endScreen(String imagePath, String message)
  {
    this.removeGameComponents();
    
    GridBagConstraints constraints = new GridBagConstraints();
    
    constraints.gridy = 0;
    
    try
    {
      BufferedImage image = ImageIO.read(new File(imagePath));
      ImageIcon icon = new ImageIcon(image);
      this.container.add(new JLabel(icon, JLabel.CENTER), constraints);
    }
    catch (IOException ioe)
    {
      this.container.add(new JLabel(message, JLabel.CENTER), constraints);
    }
                       
    constraints.gridy = 1;
    this.container.add(this.quitButton, constraints);
    
    this.pack();
    this.updateFrame();
  }
  
  /**
   * Reconfigure the frame to show that the user won the game.
   */
  public void gameWin()
  {
    this.endScreen("EndScreen/win.jpg", "You won!");
  }
  
  /**
   * Reconfigure the frame to show that the user lost the game.
   */
  public void gameLose()
  {
    this.endScreen("EndScreen/lose.jpg", "You lost.");
  }
  
  /**
   * Reconfigure the frame to show that the connection to the server was lost.
   */
  public void connectionLost()
  {
    this.endScreen("EndScreen/connection_lost.jpg", "Connection to server lost.");
  }
  
  /**
   * The main method that starts the client's GUI.
   */
  public static void main(String [] args)
  {
    ClientGUI cgui = new ClientGUI();
  }
  
}
