/**
 * Taken from Coursework 2.
 * Exception thrown by the static method newServer in the Server class when an
 * exception is encountered while trying to create a server object.
 */
public class ServerCreationException extends Exception
{
  public ServerCreationException(String errorMessage)
  {
    super(errorMessage);
  }
}
