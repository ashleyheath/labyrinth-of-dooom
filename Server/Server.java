import java.net.*;
import java.io.*;
import java.lang.Integer;

/**
 * Taken from Coursework 2.
 * Main server class.
 */
public class Server
{
  private ClientListenerThread clientListener;
  
  protected LODGame gameEngine;
  
  /**
   * Constructor.
   * @param gameEngine     the game engine.
   * @param clientListener the client listener thread.
   */
  public Server(LODGame gameEngine, ClientListenerThread clientListener)
  {
    this.gameEngine = gameEngine;
    this.clientListener = clientListener;
  }
  
  /**
   * Sets up a new server.
   * @param port the port to listen on.
   * @return     a new server instance.
   */
  public static Server newServer(int port, String mapFilePath) throws ServerCreationException
  {
    Server s;
    try
    {
      LODGame gameEngine;
      if (mapFilePath.equals(""))
      {
        gameEngine = new LODGame();
      }
      else
      {
        gameEngine = new LODGame(mapFilePath);
      }
      ClientListenerThread clientListener = ClientListenerThread.newClientListenerThread(port, gameEngine);
      clientListener.start();
      s = new Server(gameEngine, clientListener);
    }
    catch (ServerCreationException e)
    {
      throw e;
    }
    return s;
  }
  
  /**
   * Kills the client listener thread.
   */
  public void shutDown()
  {
    this.clientListener.shutDown();
  }

  /**
   * Called when run to launch the Labyrinth of Dooom server.
   * @param args any command line arguments provided.
   */
  public static void main(String [] args)
  { 
    if (args.length < 1)
    {
      System.out.println("Need the port number as the argument.");
      System.exit(1);
    }
    try
    {
      InetAddress ia = InetAddress.getLocalHost();
      System.out.println("Address: " + ia.getHostAddress());
    }
    catch (Exception e)
    {
    }
    int port = 2222;
    try
    {
      port = Integer.parseInt(args[0]);
    }
    catch (NumberFormatException e)
    {
      System.out.println("Need a number as the second argument for the port.");
      System.exit(1);
    }
    
    // This map loading code is taken from Coursework 1 and modified
    
    Console console = System.console();

    //Print the names of the avaiable maps
    System.out.println("___Available_Maps___\n");
    File directory = new File("maps/");
    String[] directoryContents = directory.list();
    if (directoryContents == null)
    {
      System.out.println("\nError: 'maps' directory does not exist.");
      System.exit(1);
    }
    for (String fileName : directoryContents)
    {
      System.out.println(" - " + fileName);
    }
    
    //Ask which map the player wants to load
    String chosenFileName = null;
    while (chosenFileName == null)
    {
      String message = "\nEnter the name of the map you would like to load, "
                       + "or hit enter to load the default map.";
      System.out.println(message);
      boolean validMapChosen = false;
      String givenMapName = console.readLine();
      if (givenMapName.equals(""))
      {
        chosenFileName = "";
        validMapChosen = true;
        break;
      }
      for (String fileName : directoryContents)
      {
        if (fileName.equals(givenMapName))
        {
          chosenFileName = fileName;
          validMapChosen = true;
          break;
        }
      }
      if (validMapChosen)
      {
        break;
      }
      else
      {
        String nameMessage = "You must enter the exact name of the map you want.";
        System.out.println(nameMessage);
      }
    }
    String filePath = "maps/" + chosenFileName;
    if (chosenFileName.equals(""))
    {
      filePath = "";
    }   
    
    Server gameServer = null;
    try
    {
      gameServer = newServer(port, filePath);
      System.out.println("Server created...");
    }
    catch (ServerCreationException e)
    {
      System.out.println(e);
      System.exit(1);
    }
    Console con = System.console();
    while (true)
    {
      String input = con.readLine();
      if (input == null)
      {
        continue;
      }
      else if (input.equals("QUIT"))
      {
        gameServer.shutDown();
        System.exit(0);
      }
    }
  }
}
