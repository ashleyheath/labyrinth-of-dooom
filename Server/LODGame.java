import java.util.Random;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Collection;
import java.util.Iterator;
import java.lang.Integer;

/**
 * Taken from Coursework 2.
 * This class controls the game logic and other such magic.
 * @author andyridge & whoever wrote it before I made it look nice...
 */
public class LODGame
{		
  private static final String TREASUREMOD = "TREASUREMOD ";
  private static final String SUCCEED = "SUCCEED";
  private static final String CHANGE = "CHANGE";
  private static final String NO_AP = "You have no action points remaining.";
  
	private ArrayList<Player> players = new ArrayList<Player>(0);
  
  private int standardDistance = 2;
  
  private LODMap lodMap;
  
 /**
  * Default Constructor.
  * This happens if there isn't a map to load.
  */
  public LODGame()
  {
  	lodMap = new LODMap();
  }
  
  /**
   * Test if there is a player at a given position on the map.
   */   
  synchronized public boolean playerAtPosition(int x, int y)
  {
    for (Player p : this.players)
    {
      if (x == p.getX() && y == p.getY())
      {
        return true;
      }
    }
    return false;
  }
    
  /**
   * Constructor that specifies the map which the game should be played on.
   * @param map The name of the file to load the map from.
   */
  public LODGame(String map)
  {
    lodMap = new LODMap(map);
  }
      
  /**
   * Puts the player in a randomised start location.
   * @param player the player to position.
   */
  synchronized public void setRandomStartLocation(Player player)
  {
    // Generate a random location
    Random random = new Random();
    int randomY = random.nextInt(lodMap.getMapHeight());
    int randomX = random.nextInt(lodMap.getMapWidth());
    
    if(lodMap.getMap()[randomY][randomX] != LODMap.WALL
       && !this.playerAtPosition(randomX, randomY))
    {
    	// If it's not a wall and there is no other player there
    	// then we can put them there
    	player.setLocation(randomX, randomY);
    }
    else
    {
    	// If it's a wall then have another go
    	setRandomStartLocation(player);
    }
  }
    
  /***** KEEPING TRACK OF PLAYERS *****/
  
  /**
   * Add a new player to the game engine.
   * Starts the player's turn if they are the only player in the game.
   * @param player the player to add.
   */  
  synchronized public void addPlayer(Player player)
  {
    this.players.add(player);
    this.setRandomStartLocation(player);
    if (this.players.size() == 1)
    {
    	this.startTurn(player);
    }
    this.sendChange(player);
  }
    
  synchronized public void removePlayer(Player player)
  {
    this.sendChange(player);
    this.players.remove(player);
  }
  
  /**
   * Return a copy of the list of players with the given player removed.
   * @param player the player to exclude from the list.
   * @return       a list of the other players.
   */ 
  synchronized public ArrayList<Player> getOtherPlayers(Player player)
  {
    @SuppressWarnings("unchecked")
    ArrayList<Player> others = (ArrayList<Player>)this.players.clone();
    others.remove(player);
    return others;
  }
  
  /**
   * Returns an ArrayList of players within a certain distance of the given
   * player.
   * @param player the player to check distances against.
   * @return       an ArrayList of players within the distance given of the
   *               given player.
   */
  synchronized public ArrayList<Player> getPlayersWithinDistance(Player player, 
  																															int distance)
  {
    ArrayList<Player> others = this.getOtherPlayers(player);
    int index = 0;
    while (index < others.size())
    {
      Player p = others.get(index);
      // Check the four boundaries
      boolean top = p.getY() >= player.getY() - distance;
      boolean bottom = p.getY() <= player.getY() + distance;
      boolean left = p.getX() >= player.getX() - distance;
      boolean right = p.getX() <= player.getX() + distance; 
      if (!(top && bottom && left && right))
      {
        others.remove(p);
      }
      // Work out if the player is in the square field of vision.
		  else if (Math.abs(player.getX()-p.getX())
		           + Math.abs(player.getY()-p.getY()) > distance + 1) 
		  {
				// It's outside the FoV so we don't know what it is.
				others.remove(p);
		  }
		  else
		  {
		    index++;
		  }
    }
    return others;
  }  
  
  /**
   * Returns an ArrayList of players that can see the player.
   * @param player the player to check if they can be seen.
   * @return       an ArrayList of players that can see the given player.
   */  
  synchronized public ArrayList<Player> getPlayersThatCanSeePlayer(Player player)
  {
    ArrayList<Player> others = this.getOtherPlayers(player);
    int sd = this.standardDistance;
    int index = 0;
    while (index < others.size())
    {
      Player p = others.get(index);
      int l = p.getLantern();
      boolean top = p.getY() >= player.getY() - sd - l;
      boolean bottom = p.getY() <= player.getY() + sd + l;
      boolean left = p.getX() >= player.getX() - sd - l;
      boolean right = p.getX() <= player.getX() + sd + l;  
      if (!(top && bottom && left && right))
      {
        others.remove(p);
      }
      // Work out if the player is in the square field of vision.
		  else if (Math.abs(p.getX()-player.getX())
		           + Math.abs(p.getY()-player.getY()) > sd + l + 1) 
		  {
				// It's outside the FoV so we don't know what it is.
				others.remove(p);
		  }
		  else
		  {
		    index++;
		  }
    }
    return others;
  }
    
  /***** COMMUNICATION WITH PLAYER CLIENTS *****/  
  
  /**
   * The most basic method of communication between game engine and client.
   * @param player the player to send the code to.
   * @param code   the code to send.
   */
  synchronized public void codeToPlayer(Player player, String code)
  {
    player.sendMessageToPlayer(code);
  }  
    
  synchronized public void messageToPlayersWithinStandardDistance(Player player,
                                                                  String message)
  {
    ArrayList<Player> others = this.getPlayersWithinDistance(player, 
                                                             this.standardDistance);
    for (Player p : others)
    {
    	this.messageToPlayer(p, message);
    }
  }
    
  synchronized public void codeToPlayersThatCanSeePlayer(Player player, 
                                                         String message)
  {
    ArrayList<Player> others = this.getPlayersThatCanSeePlayer(player);
    for (Player p : others)
    {
    	this.codeToPlayer(p, message);
    }
  }
    
  synchronized public void serverMessageToPlayersThatCanSeePlayer(Player player,
                                                                  String message)
  {
    this.codeToPlayersThatCanSeePlayer(player, "MESSAGE * FROM SERVER: " + message);
  }
    
  synchronized public void codeToOtherPlayers(Player player, String code)
  {
    ArrayList<Player> others = this.getOtherPlayers(player);
    for (Player p : others)
    {
    	this.codeToPlayer(p, code);
    }
  }
    
  synchronized public void messageToPlayer(Player player, String message)
  {
    this.codeToPlayer(player, "MESSAGE " + message);
  }
    
  synchronized public void serverMessageToPlayer(Player player, String message)
  {
    this.messageToPlayer(player, "* FROM SERVER: " + message);
  }
    
  synchronized public void messageToOtherPlayers(Player player, String message)
  {
    this.codeToOtherPlayers(player, "MESSAGE " + message);
  }
    
  synchronized public void serverMessageToOtherPlayers(Player player, 
                                                       String message)
  {
    this.messageToOtherPlayers(player, "* FROM SERVER: " + message);
  }
    
  synchronized public void sendChange(Player player)
  {
    this.codeToPlayersThatCanSeePlayer(player, CHANGE);
  }
  
  synchronized public void sendChangeToGroup(ArrayList<Player> players)
  {
    for (int i = 0; i < players.size(); i++)
    {
      this.codeToPlayer(players.get(i), CHANGE);
    }
  }
    
  /***** TURN HANDLING *****/
  
  /**
   * Start the given player's turn.
   * @param player the player who's turn to start.
   */  
  synchronized public void startTurn(Player player)
  {
    this.codeToPlayer(player, "STARTTURN");
    this.serverMessageToPlayer(player, "It is now your turn.");
    this.serverMessageToOtherPlayers(player, player.getName()
    	                               + "'s turn has begun.");
    player.setAp(6 - (player.getLantern() + player.getSword()
    	           + player.getArmour()));
  }
  
  /** 
   * Once a player has performed an action the game needs to move onto the next
   * turn to do this the game needs to check for a win and then test to see if 
   * the current player has more AP left.
   * @param player the player to check if their turn is over.
   */
  synchronized public void advanceTurn(Player player)
  {
  	// Check if the player has won.
  	if (player.getGold() >= lodMap.getGoal() && 
   		  lodMap.getMap()[player.getY()][player.getX()] == LODMap.EXIT) 
   	{
    	this.codeToPlayer(player, "WIN");
      this.codeToOtherPlayers(player, "LOSE");
		  System.exit(0);
	  }
   	else
   	{
   		if(this.isTurnFinished(player))
   		{
   		  // If the turn has finished then end the turn and start a new one
   	    this.codeToPlayer(player, "ENDTURN");
   	    this.serverMessageToPlayer(player, "That's the end of your turn.");
   	    this.serverMessageToOtherPlayers(player, player.getName()
   	                                     +"'s turn ended.");
        int nextIndex = this.players.indexOf(player) + 1;
   	    if (nextIndex == this.players.size())
   	    {
   	      nextIndex = 0;
   	    }
   	    // Avoid out of bounds if there are no more players in the game
   	    if (this.players.size() > 0)
    		{
   			  Player nextPlayer = this.players.get(nextIndex);
    			this.startTurn(nextPlayer);
    		}
    	}
    }
  }
    
  /**
   * Handles the client message ENDTURN
   * Sets the player's AP to zero, notifies players of the end of turn.
   * @param player the player who's turn should end.
   */
  synchronized public void clientEndTurn(Player player) 
  {
		player.setAp(0);
	  this.advanceTurn(player);	
		this.codeToPlayer(player, "ENDTURN");
		this.serverMessageToPlayer(player, "Your turn has ended.");
		this.serverMessageToOtherPlayers(player, player.getName()
		                                 + "'s turn ended.");
  }
    
  /**
   * Check if the turn as finished (i.e. if the player has any AP left)
   * @return true if the player has AP left, false otherwise.
   */
  synchronized private boolean isTurnFinished(Player player)
  {
  	return (player.getAp() == 0);
  }
 
  /***** GAME COMMANDS AND LOGIC *****/
    
  /**
   * Handles the client message LOOK
   * Shows the portion of the map that the player can currently see.
   * Then calls the renderhint method for the player.
   * @param player the player requesting the LOOKREPLY.
   */
  synchronized public void clientLook(Player player) 
  {
  	String map = "";
    	
   	// Work out how far the player can see
    int distance = this.standardDistance + player.getLantern();
	   
	  // Iterate through the rows.
	  for (int i = -distance; i <= distance; ++i) 
	  {
	  	map = map + "\n";
	   	String line = "";
		
	    // Iterate through the columns.
		  for (int j = -distance; j <= distance; ++j) 
		  {
		    char content = '?';
			  // Work out which location is next.
		    int targetX = player.getX() + j;
		    int targetY = player.getY() + i;
			    
		    // Work out what is in the square field of vision.
		    if (Math.abs(i) + Math.abs(j) > distance + 1) 
		    {
				  // It's outside the FoV so we don't know what it is.
				  content = 'X';
		    } 
		    else if ((targetX < 0) || (targetX >= lodMap.getMapWidth()) ||
				         (targetY < 0) || (targetY >= lodMap.getMapHeight())) 
			  {	
	        // It's outside the map, so just call it a wall.
			    content = '#';
		    }
			  else
			  { 
			    // Look up and see what's on the map
					switch (lodMap.getMap()[targetY][targetX]) 
					{
						case LODMap.EMPTY: 		content = '.'; break;
						case LODMap.HEALTH: 	content = 'H'; break;
						case LODMap.LANTERN: 	content = 'L'; break;
						case LODMap.SWORD: 		content = 'S'; break;
						case LODMap.ARMOUR: 	content = 'A'; break;
						case LODMap.EXIT: 		content = 'E'; break;
						case LODMap.WALL: 		content = '#'; break;
						case LODMap.GOLD:		content = 'G'; break;
					  default : 
					    // This shouldn't happen
					    System.err.println("Invalid map location : [" + targetY + "][" + 
					                        targetX + "] = " + 
					                        lodMap.getMap()[targetY][targetX]);
					    System.exit(1);
					}
			  }	
				// Add to the line
				line += content;
		  }
		  // Send a line of the look message 
		  map += line;
	  }
    this.codeToPlayer(player, "LOOKREPLY" + map + "\n");
    this.renderHint(player);
  }
    
  /**
   * Sends the player the render hint code.
   * @param player the player to send the render hints to.
   */
  synchronized private void renderHint(Player player)
  {
    int distance = this.standardDistance + player.getLantern();
    ArrayList<Player> nearbyPlayers;
    nearbyPlayers = this.getPlayersWithinDistance(player, distance);
    this.codeToPlayer(player, "RENDERHINT " + nearbyPlayers.size());
    for (Player p : nearbyPlayers)
    {
      Integer relativeX = new Integer(player.getX() - p.getX());
      Integer relativeY = new Integer(player.getY() - p.getY());
      String renderHint = relativeX.toString() + " "
                          + relativeY.toString() + " PLAYER";
      this.codeToPlayer(player, renderHint);
    }
  }
    
  /**
   * Sends a message from one client to all others within the standard map
   * distance of that player.
   * @param player    the player who issued the shout.
   * @param message 	The message to be shouted
   */
  synchronized public void clientShout(Player player, String message) 
  {
    this.messageToPlayersWithinStandardDistance(player, message);
  }
    
  /**
   * Handles the client message PICKUP.
   * Generally it decrements AP, and gives the player the item they picked up
   * Also removes the item from the map
   * @param player the player trying to pickup.
   */
  synchronized public void clientPickup(Player player) 
  {
	  String failMessage = "FAIL ";
		
		// Can only pick up in you have action points left
		if (player.getAp() > 0) 
		{
		  // Check that there is something to pick up
		  switch (lodMap.getMap()[player.getY()][player.getX()]) 
		  {
			  case LODMap.EXIT : // Can't pick up the exit
			  case LODMap.EMPTY : // Nothing to pick up
			    failMessage += "Nothing to pick up";
			    this.codeToPlayer(player, failMessage);
			    this.serverMessageToPlayer(player, "There is nothing to pickup.");
			    return;
			    	
			  case LODMap.HEALTH :
					// Costs the rest of your actions
					player.setAp(0);
					// Remove from the map
					lodMap.setMapCell(player.getY(), player.getX(), LODMap.EMPTY);
					// Add one to health...
					player.incrementHealth();
					// ... notify the client ...
					this.advanceTurn(player);
					this.sendChange(player);
					this.codeToPlayer(player, SUCCEED + " PICKUP HELATH");
					return;
					
			  case LODMap.LANTERN :
			    // Can pick up if we don't have one
					if (player.getLantern() == 0) 
					{
					  // Costs one action point
					  player.decrementAp();
					  // Remove from the map
					  lodMap.setMapCell(player.getY(), player.getX(), LODMap.EMPTY);
					  // ... give them a lantern ...
					  player.setLantern(1);
					  this.advanceTurn(player);
					  this.sendChange(player);
					  this.codeToPlayer(player, SUCCEED + " PICKUP LANTERN");
					  return;
					} 
					else 
					{
					  failMessage += "Already have a lantern";
					  this.codeToPlayer(player, failMessage);
					  this.serverMessageToPlayer(player, "You already have a lantern.");
					  return;
					}
					
			  case LODMap.SWORD :
			    // Does almost exactly the same thing as picking up a lantern		
					if (player.getSword() == 0) 
					{
					  player.decrementAp();
					  this.lodMap.setMapCell(player.getY(), player.getX(), LODMap.EMPTY);
					  player.setSword(1);
					  this.advanceTurn(player);
					  this.sendChange(player);
					  this.codeToPlayer(player, SUCCEED + " PICKUP SWORD");
					  return;
					} 
					else 
					{
					  failMessage += "Already have a sword";
					  this.codeToPlayer(player, failMessage);
					  this.serverMessageToPlayer(player, "You already have a sword.");
					  return;
					}
					
			    case LODMap.ARMOUR :
					// Similar again
					if (player.getArmour() == 0) 
					{
					  player.decrementAp(); 
					  this.lodMap.setMapCell(player.getY(), player.getX(), LODMap.EMPTY);
					  player.setArmour(1);
					  this.advanceTurn(player);
					  this.sendChange(player);
					  this.codeToPlayer(player, SUCCEED + " PICKUP ARMOUR");
					  return;
					} 
					else 
					{
					  failMessage += "Already have armour";
					  this.codeToPlayer(player, failMessage);
					  this.serverMessageToPlayer(player, "You already have some armour.");
					  return;
					}
					
			    case LODMap.GOLD:
				    // Costs one action point
				    player.decrementAp();
				    // Remove from the map
				    this.lodMap.setMapCell(player.getY(), player.getX(), LODMap.EMPTY);
				    // Add to the amount of treasure
				    player.addGold(1); 
				    
				    this.sendChange(player);
					  this.codeToPlayer(player, SUCCEED + " PICKUP GOLD");
					  this.codeToPlayer(player, TREASUREMOD + "1");
					  this.advanceTurn(player);
					  return;  
				
			    default:
				    // This shouldn't happen
				    System.err.println("Pickup at strange map location : [" +
				                       player.getY() + "][" + player.getX() + "] = "
				                       + lodMap.getMap()[player.getY()][player.getX()]);
				    System.exit(1);
		    }
		 } 
		 else 
		 {
		   failMessage += "No action points left";
		   this.serverMessageToPlayer(player, failMessage);
		 }
  }
    
  /**
   * Handles the client message MOVE
   * Move the player in the specified direction - assuming there isn't a wall or
   * another player in the way.
   * @param palyer    the player trying to move.
   * @param direction The direction (NESW) to move the player
   */
  synchronized public void clientMove(Player player, char direction) 
  {
		String failMessage = "FAIL ";
		/* Can only move in the player's turn and if they have action points
		   remaining. */
		if (player.getAp() > 0) 
		{
		  // Work out where the move would take the player
		  int targetX = player.getX();
		  int targetY = player.getY();
		    
		  switch (direction) 
		  {
			  case 'N' : --targetY; break;
			  case 'S' : ++targetY; break;
			  case 'E' : ++targetX; break;
			  case 'W' : --targetX; break;
			    
			  default :  // Shouldn't happen
			    System.err.println("Internal error in connection base.");
			    System.err.println("'" + direction + "' is not a direction.");
				  System.exit(1);
		  } 
		  
		  	// Ensure the target tile doesn't contain another player
			  if (this.playerAtPosition(targetX, targetY))
		    {
		      String message = "You can't walk into tiles occupied by other players.";
		      failMessage += "Can't move into another player";
				  this.codeToPlayer(player, failMessage);
			    this.serverMessageToPlayer(player, message);
			    return;
		    }
		  
		  // Ensure that the movement is within the bounds of the map
		  if ((targetX >= 0) && (targetX < lodMap.getMapWidth()) &&
		    	(targetY >= 0) && (targetY < lodMap.getMapHeight())) 
		  {
		    // The move must not be into a wall
				if (this.lodMap.getMap()[targetY][targetX] != LODMap.WALL) 
				{
				  // Costs one action point
					player.decrementAp();
					// Record other players who can see the player before they move
					ArrayList<Player> couldSeePlayer = this.getPlayersThatCanSeePlayer(player);
          // Move the player
				  player.setX(targetX);
					player.setY(targetY);
					// Notify the client of the success
					
					this.codeToPlayer(player, SUCCEED);
					/* Send a CHANGE code to all players that could see the player before
					   they moved and all those that could see the player after they
					   moved. */		
					ArrayList<Player> canSeePlayer = this.getPlayersThatCanSeePlayer(player);
					// Use a set to avoid sending duplicate CHANGE messages
					HashSet<Player> playersToNotify = new HashSet<Player>();
					playersToNotify.addAll(couldSeePlayer);
					playersToNotify.addAll(canSeePlayer);

					this.sendChangeToGroup(new ArrayList<Player>(playersToNotify));
					
					this.advanceTurn(player);
					return;
			  }
				else
				{
				  failMessage += "Can't move into a wall";
				  this.codeToPlayer(player, failMessage);
					this.serverMessageToPlayer(player, "You can't move into a wall.");
					return;
				}

				
		  } 
		  else 
		  {
				// Needs to be the same as above or otherwise people will know where
				// the edges of the labyrinth are.
				failMessage += "Can't move into a wall";
				this.codeToPlayer(player, failMessage);
			  this.serverMessageToPlayer(player, "You can't move into a wall.");		
			  return;	
		  }
		} 
		else 
		{
			failMessage += "No action points left";
			this.codeToPlayer(player, failMessage);
			this.serverMessageToPlayer(player, NO_AP);
			return;
		}
  }
 
  /**
   * Handles the client message ATTACK (Not yet implemented)
   * @param player    the attacking player.
   * @param direction	The direction in which to attack
   */
  synchronized public void clientAttack(Player player, char direction) 
  {
  	String failMessage = "FAIL ";
   	/* Can only attack if it is this player's turn and they have action points 
   	   left. */
	  if (player.getAp() > 0) 
	  {
	    // Work out which square we're targeting
		  int targetX = player.getX();
		  int targetY = player.getY();
		      
		  switch (direction) 
		  {
			  case 'N' : --targetY; break;
			  case 'S' : ++targetY; break;
			  case 'E' : ++targetX; break;
			  case 'W' : --targetX; break;
			      
			  default :  // Shouldn't happen
			    System.err.println("Internal error in connection base.");
			    System.err.println("'" + direction + "' is not a direction.");
					System.exit(1);
		  }
		      
		 /** 
		  * TODO .... This code does not need to be filled in until Coursework 3!
		  * 
		  * 1. Work out which player the attack is on...
		  * 
		  * 2. Have you hit the target? - hint, you might want to make the chance of 
		  *    a successful attack 75%?
		  * 2.1 if the player has hit the target then hp of the target should be 
		  *     reduced based on this formula...
		  *     damage = 1 + player.sword - target.armour 
		  * 
	    *     i.e. the damage inflicted is 1 + 1 if the player has a sword and - 1
	    *          if the target has armour.
      *     Player and target are informed about the attack as set out in the 
	    *     wire_spec
		  * 2.2 if the player has missed the target then nothing happens. 
		  *     Optionally, the player and target can be informedabout the failed 
		  *     attack 
		  */
			this.codeToPlayer(player, failMessage);
			String message = "Attacking not implemented.";
		  this.serverMessageToPlayer(player, message);		    
		} 
		else 
		{
		  failMessage += "No action points left";
		  this.codeToPlayer(player, failMessage);
		  this.serverMessageToPlayer(player, NO_AP);	
		}
  }
    
  /**
   * Passes the goal back for the server threads.
   * @return The current goal
   */
  synchronized public int getGoal()
  {
    int goal = lodMap.getGoal();
    if (goal < 0)
    {
      goal = goal * -1;
    }
  	return goal;
  }
}
