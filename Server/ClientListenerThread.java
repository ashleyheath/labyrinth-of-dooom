import java.net.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Taken from Coursework 2.
 * Listens for clients connecting on the server socket.
 */
public class ClientListenerThread extends Thread
{
  private ServerSocket listenSocket;
  private LODGame gameEngine;
  private ArrayList<ServerThread> clientConnections = new ArrayList<ServerThread>(0);
  
  /**
   * Constructor.
   * @param serverSock the server socket.
   * @param gameEngine the game engine.
   */
  public ClientListenerThread(ServerSocket serverSock, LODGame gameEngine)
  {
    this.listenSocket = serverSock;
    this.gameEngine = gameEngine;
  } 
  
  /**
   * Sets up a new client listener thread.
   * @param port       the port to listen on.
   * @param gameEngine the game engine.
   * @return           a new ClientListenerThread instance.
   */
  public static ClientListenerThread newClientListenerThread(int port,
                                                             LODGame gameEngine)
                                                             throws ServerCreationException
  {
    ClientListenerThread clt;
    ServerSocket serverSock;
    try
    {
      serverSock = new ServerSocket(port);
      clt = new ClientListenerThread(serverSock, gameEngine);
    }
    catch (BindException e)
    {
      throw new ServerCreationException("Could not bind to port " + port
                                        + ", try a different one.");
    }
    catch (IOException e)
    {
      throw new ServerCreationException("IO error encountered.");
    }
    return clt;
  }
    
  public void run()
  {
    while (true)
    {
      Socket sock = null;
      try
      {
        sock = this.listenSocket.accept();
        System.out.println("Connection made...");
      }
      catch (IOException ioe)
      {
        System.out.println("Error: Could not create client socket.");
      }
      //Create a new thread to deal with the client
      if (sock != null)
      {
        try
        {
          ServerThread newServerThread = new ServerThread(sock, 
          																								this.gameEngine);
          newServerThread.start();
          this.clientConnections.add(newServerThread);
        }
        catch (IOException e)
        {
          System.out.println("ERROR: Could not create client thread.");
        }
      }
    }
  }
  
  /**
   * Try and safely close the listening socket and all sockets created by
   * spawned ServerThreads.
   */
  public void shutDown()
  {
    try
    {
      this.listenSocket.close();
    }
    catch (IOException e)
    {
      System.out.println("Could not close server socket.");
    }
    for (ServerThread sThread : this.clientConnections)
    {
      sThread.kill();
    }
  }
}
