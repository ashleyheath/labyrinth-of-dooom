import java.net.*;
import java.io.*;

/**
 * Taken from Coursework 2.
 * A thread that handles communications to and from an individual client.
 */
public class ServerThread extends Thread
{
  // CODES
  private static final String SUCCEED = "SUCCEED";
  private static final String FAIL = "FAIL";
  private static final String ENDTURN = "ENDTURN";
  private static final String SHOUT_ = "SHOUT ";
  private static final String PICKUP = "PICKUP";
  private static final String MOVE_ = "MOVE ";
  private static final String ATTACK_ = "ATTACK ";
  private static final String HELLO_ = "HELLO ";
  private static final String LOOK = "LOOK";
  private static final String PING = "PING";
  private static final String PONG = "PONG";

  private Socket sock;
  private LODGame gameEngine;
  private Player threadPlayer;
  private BufferedReader input;
  private PrintWriter output;
  
  /**
   * Constructor.
   * @param sock       the socket to the client.
   * @param gameEngine the reference to the game engine.
   */
  public ServerThread(Socket sock, LODGame gameEngine) throws IOException
  {
    this.sock = sock;
    this.gameEngine = gameEngine;
    this.input = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
    this.output = new PrintWriter(this.sock.getOutputStream(), true);
    this.threadPlayer = new Player(this.output);
    this.gameEngine.addPlayer(this.threadPlayer);
    // Inform client of the amount of gold required to win the game
    int gold = gameEngine.getGoal();
    this.gameEngine.codeToPlayer(this.threadPlayer, "GOAL " + gold);
    this.gameEngine.serverMessageToPlayer(this.threadPlayer, "You need "
                                          + gold + " gold to win.");                                
  }
  
  /**
   * Try and safely close the socket to the client.
   */
  public void kill()
  {
    try
    {
      this.threadPlayer.setAp(0);
      this.gameEngine.advanceTurn(this.threadPlayer);
      this.gameEngine.removePlayer(this.threadPlayer);
      this.sock.close();
    }
    catch (IOException e)
    {
      System.out.println("ERROR: Could not close socket in thread.");
    }
  }

  
  public void sendString(String message)
  {
    this.output.println(message);
    this.output.flush();
  }
  
  public void informInvalidCommand(String messageFromClient)
  {
    this.gameEngine.serverMessageToPlayer(this.threadPlayer, "'" 
                                          + messageFromClient
                                          + "' is not a valid command");
  }
  
  /**
   * Wait for a transmission from the client and then take the appropriate
   * action.
   */
  public void run()
  {
    boolean firstRun = true;
    try
    {
      String messageFromClient;
      while ((messageFromClient = this.input.readLine()) != null)
      {
        // If not a ping print what the client sent
        if (!messageFromClient.equals(PING))
        {
          System.out.println("From " + this.threadPlayer.getName() + ": "
                              + messageFromClient);
        }
        
        // If this is the first run see if we got the HELLO code
        if (firstRun)
        {
          if (messageFromClient.equals(PING))
          {
            this.sendString(PONG);
            continue;
          }
          else if (messageFromClient.equals(HELLO_))
          {
            this.sendString(HELLO_ + this.threadPlayer.getName());
            firstRun = false;
            continue;
          }
          else if (messageFromClient.length() > 6
                   && messageFromClient.indexOf(HELLO_) == 0)
          {
            int messageLen = messageFromClient.length();
            String username = messageFromClient.substring(6, messageLen);
            this.threadPlayer.setName(username);
            String name = this.threadPlayer.getName();
            this.gameEngine.codeToPlayer(this.threadPlayer, 
                                         HELLO_ + name);
            firstRun = false;
            continue;
          }
          else
          {
            firstRun = false;
          }
        }
        
        // ARE WE ALIVE?
        if (!this.threadPlayer.isDead())
        {
          if (messageFromClient.equals(LOOK))
          {
            this.gameEngine.clientLook(this.threadPlayer);
          }
          // MOVE
          else if (messageFromClient.indexOf(MOVE_) == 0
                   && messageFromClient.length() >= 6)
          {
            char direction = messageFromClient.charAt(5);
            // Check that direction code is valid
            if (direction == 'N' || direction == 'S' || direction == 'E'
                || direction == 'W')
            {
              this.gameEngine.clientMove(this.threadPlayer, direction);
            }
            else
            {
              this.informInvalidCommand(messageFromClient);
            }
          }
          // ATTACK
          else if (messageFromClient.indexOf(ATTACK_) == 0)
          {
            char direction = messageFromClient.charAt(7);
            if (direction == 'N' || direction == 'S' || direction == 'E'
                || direction == 'W')
            {
              this.gameEngine.clientAttack(this.threadPlayer, direction);
            }
            else
            {
              this.informInvalidCommand(messageFromClient);
            }
          }
          // PICKUP
          else if (messageFromClient.equals(PICKUP))
          {
            this.gameEngine.clientPickup(this.threadPlayer);
          }
          
          // ENDTURN
          else if (messageFromClient.equals(ENDTURN))
          {
            this.threadPlayer.setAp(0);
            this.gameEngine.advanceTurn(this.threadPlayer);
          }
          /* Codes that we take care of later (as they can be sent by dead
             clients). */
          else if (messageFromClient.equals(PING))
          {
          }
          else if (messageFromClient.indexOf(SHOUT_) == 0)
          {
          }
          // Otherwise send the client a message saying that the 
          else
          {
            this.gameEngine.codeToPlayer(this.threadPlayer,
                                         "FAIL Invalid code.");
            this.informInvalidCommand(messageFromClient);
          }
        }
        
        // COMMANDS WE CAN DO IF WE ARE DEAD
        if (messageFromClient.equals(PING))
        {
          this.sendString(PONG);
        }
        // Shout
        else if (messageFromClient.indexOf(SHOUT_) == 0)
        {
          int mLen = messageFromClient.length();
          String message = "From " + threadPlayer.getName() + ":" + 
                           messageFromClient.substring(5, mLen);
          this.gameEngine.messageToPlayersWithinStandardDistance(this.threadPlayer,
                                                                 message);
        }          
      }
    }
    catch (IOException e)
    {
      // Occurs if the client disconnects
    }
    /* If a client disconnects then remove them from the players list and
       move on to the next player. */
    this.kill();
    System.out.println("Client '" + this.threadPlayer.getName()
                       + "' has disconnected.");
  }
}
